# ElderFuthark

# TODO

- Accept title and question for inquiry

This would be accomplished by hashing the input to get an integer, creating a random seed (using Erlang's `:random.seed` function) and then using a random algorithm to choose runes.

Drawing from the Enum may have to be quite manual, as `Enum.take_random` doesn't expose any of the random module's workings, and depending on `:random`'s global state will be risky.


- Take login and auth
- Store previous inquiries
