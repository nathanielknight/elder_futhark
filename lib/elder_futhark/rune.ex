defmodule ElderFuthark.Rune do
  @enforce_keys [:name, :codepoint, :image, :meaning]
  defstruct [:name, :codepoint, :image, :meaning]
end
