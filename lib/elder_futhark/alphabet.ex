defmodule ElderFuthark.Alphabet do
  alias ElderFuthark.Rune

  def draw(n) do
    Enum.take_random(all(), n)
  end

  def all(),
    do: [
      %Rune{
        name: "Fehu",
        codepoint: "ᚠ",
        image: "Domestic cattle, wealth",
        meaning: ~s"""
        Accrued wealth and power.
        """
      },
      %Rune{
        name: "Uruz",
        codepoint: "ᚢ",
        image: "Auroch, a wild ox",
        meaning: ~s"""
        Power that cannot be controlled.
        """
      },
      %Rune{
        name: "Thurisaz",
        codepoint: "ᚦ",
        image: "Thorn or giant",
        meaning: ~s"""
        Reactive force, directed force of destruction and defense, conflict.
        """
      },
      %Rune{
        name: "Ansuz",
        codepoint: "ᚨ",
        image: "The ancestral god",
        meaning: ~s"""
        A revealing message or insight, communication.
        Blessings, the taking of advice.
        """
      },
      %Rune{
        name: "Raido",
        codepoint: "ᚱ",
        image: "A wagon or chariot",
        meaning: ~s"""
        Travel, both in physical terms and those of lifestyle direction.
        Seeing the right move for you to make and deciding upon it.
        """
      },
      %Rune{
        name: "Kenaz",
        codepoint: "ᚲ",
        image: "A beacon or torch",
        meaning: ~s"""
        Vision, revelation, knowledge, creativity, inspiration, technical ability.
        Vital fire of life, harnessed power, fire of transformation and regeneration.
        Power to create your own reality, the power of light.
        Open to new strength, energy, and power now.
        Passion, sexual love.
        """
      },
      %Rune{
        name: "Gebo",
        codepoint: "ᚷ",
        image: "A gift",
        meaning: ~s"""
        Gifts, both in the sense of sacrifice and of generosity, indicating balance.
        All matters in relation to exchanges, including contracts, personal relationships, partnerships,
        honour, and standing.
        """
      },
      %Rune{
        name: "Wunjo",
        codepoint: "ᚹ",
        image: "Joy",
        meaning: ~s"""
        Joy, comfort, pleasure.
        Fellowship, harmony, prosperity.
        Recognition of worth.
        """
      },
      %Rune{
        name: "Hagalaz",
        codepoint: "ᚺ",
        image: "Hail",
        meaning: ~s"""
        Wrath of nature, destructive, uncontrolled forces, especially the weather, or within the unconscious.
        Tempering, testing, trial.
        Controlled crisis, leading to completion, inner harmony.
        """
      },
      %Rune{
        name: "Nauthiz",
        codepoint: "ᚾ",
        image: "Need",
        meaning: ~s"""
        Delays, restriction.
        Resistance leading to strength, innovation, need-fire (self-reliance).
        Distress, confusion, conflict, and the power of will to overcome them.
        Endurance, survival, determination.
        """
      },
      %Rune{
        name: "Isa",
        codepoint: "ᛁ",
        image: "Ice",
        meaning: ~s"""
        A challenge or frustration.
        Psychological blocks to thought or activity, including grievances.
        Standstill, or a time to turn inward and wait for what is to come, or to seek clarity.
        """
      },
      %Rune{
        name: "Jera",
        codepoint: "ᛃ",
        image: "A year, a good harvest",
        meaning: ~s"""
        The results of earlier efforts are realized.
        The promise of success earned.
        Life cycle, cyclical pattern of the universe.
        """
      },
      %Rune {
        name: "Eihwaz",
        codepoint: "ᛇ",
        image: "A yew tree",
        meaning: ~s"""
        Strength, reliability, dependability, trustworthiness.
        The driving force to build, providing motivation and a sense of purpose.
        """
      },
      %Rune {
        name: "Pertho",
        codepoint: "ᛈ",
        image: "A lot cup",
        meaning: ~s"""
        A secret, or hidden thing.
        """
      },
      %Rune {
        name: "Algiz",
        codepoint: "ᛉ",
        image: "Elk",
        meaning: ~s"""
        Protection, and guarding others.
        Keeping what you have.
        """
      },
      %Rune {
        name: "Sowilo",
        codepoint: "ᛊ",
        image: "The Sun",
        meaning: ~s"""
        Success and goals achieved.
        Vitality and health.
        Wholeness and power, the opportunity for positive, directed change.
        """
      },
      %Rune {
        name: "Tehwaz",
        codepoint: "ᛏ",
        image: "Tyr, the sky god",
        meaning: ~s"""
        Honor, justice, leadership, and authority.
        Self knowledge, good judgement, and the will to apply them selflessly.
        """
      },
      %Rune {
        name: "Berkano",
        codepoint: "ᛒ",
        image: "Berchta, the birch godess",
        meaning: ~s"""
        Birth, growth, regeneration, and healing.
        New beginnings and the light of spring.
        """
      },
      %Rune {
        name: "Ehwaz",
        codepoint: "ᛖ",
        image: "A pair of horses",
        meaning: ~s"""
        Movement, change, and progress.
        Partnership and teamwork.
        """
      },
      %Rune {
        name: "Mannaz",
        codepoint: "ᛗ",
        image: "Humankind",
        meaning: ~s"""
        The self, awareness, intelligence.
        All people, relationships, social ties, relations, and interactions.
        """
      },
      %Rune {
        name: "Laguz",
        codepoint: "ᛚ",
        image: "A body of water",
        meaning: ~s"""
        Flowing water, finding the easy course, conforming to its vessel.
        """
      },
      %Rune {
        name: "Ingwaz",
        codepoint: "ᛜ",
        image: "Fertility",
        meaning: ~s"""
        Gestation, development, internal growth.
        Simplicity and clarity allowing for focus.
        """
      },
      % Rune {
        name: "Dagaz",
        codepoint: "ᛞ",
        image: "Dawn",
        meaning: ~s"""
        Balance and clarity.
        Preparation, planning, awakening, or initiation.
        """
      },
      % Rune {
        name: "Othala",
        codepoint: "ᛟ",
        image: "An ancestral home",
        meaning: ~s"""
        Legacy, inherited property, a house or home.
        Origins and core values.
        Safety, belonging, and abundance.
        """
      }

    ]

end
