defmodule ElderFutharkWeb.CastingController do
  use ElderFutharkWeb, :controller

  alias ElderFuthark.Alphabet

  def index(conn, _params) do
    render(conn, "index.html", runes: ["1", "2", "3"])
  end



  def single(conn, _params) do
    runes = Alphabet.draw(1)
    render(conn, "single_casting.html", runes: runes)
  end

  def triple(conn, _params) do
    [past, present, future] = Alphabet.draw(3)
    assigns = %{past_rune: past, present_rune: present, future_rune: future}
    IO.inspect(assigns)
    render(conn, "triple_casting.html", assigns)
  end
end
