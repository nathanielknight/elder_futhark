defmodule ElderFutharkWeb.Router do
  use ElderFutharkWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ElderFutharkWeb do
    pipe_through :browser

    get "/", CastingController, :index
    get "/single", CastingController, :single
    get "/triple", CastingController, :triple
  end

  # Other scopes may use custom stacks.
  # scope "/api", ElderFutharkWeb do
  #   pipe_through :api
  # end
end
