defmodule ElderFuthark.TestRune do
  use ExUnit.Case, async: true
  alias ElderFuthark.Rune

  test "Can create and access" do
    r = %Rune{name: "A", codepoint: "A", image: "An a frame", meaning: "The first letter"}
    assert r.name == "A"
    assert r.codepoint == "A"
    assert r.image == "An a frame"
  end
end
